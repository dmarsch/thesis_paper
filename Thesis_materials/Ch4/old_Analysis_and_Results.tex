There are three well defined methods for determining success in extrapolation approaches. First, a few terms need to be defined: % make the terms a set of bullet points
\begin{itemize}
\item Success: Both the prediction and truth pixels are above the threshold values.
\item False Alarm: The prediction pixel is above the threshold value but the truth pixel is below the threshold value.
\item Miss: The prediction pixel is below the threshold value but the truth pixel is above the threshold value.
\end{itemize}
The threshold value used in this study was $5 \ mm \ h^{-1}$. Based on these definitions the methods of determining success can be given. The first statistic is called Probability Of Detection (POD), the second is called False Alarm Ratio (FAR), and the third is the Critical Success Index (CSI) \citep{Donaldson++75}. These methods can be explained mathematically as:
\begin{equation}
POD = \frac{n_{success}}{n_{success} + n_{failure}}
\label{e:POD}
\end{equation}
\begin{equation}
FAR = \frac{n_{false\ alarm}}{n_{success} + n_{false\ alarm}}
\label{e:FAR}
\end{equation}
\begin{equation}
CSI = \frac{n_{success}}{n_{success} + n_{failure} + n_{false\ alarm}}
\label{e:CSI}
\end{equation}

Two separate case studies were examined: 
\begin{itemize}
\item Hurricane Harvey, a hurricane that had the most effect on Texas in the United States which “was the most significant tropical cyclone rainfall  event in the United States in terms of scope and peak rainfall amounts”, over a two day period of August 28th and 29th \citep{Blake++18}.
\item Hurricane Irma, a hurricane that had the most effect on Florida in the United States which “was one of the strongest and costliest hurricanes in the Atlantic basin”, over a two day period of September 10th and 11th \citep{Cangialosi++18}.
\end{itemize}

The results from the proposed approach described in Chapter~\ref{chap:Method} will be compared against the TREC approach (described in section ~\ref{s:Cross Correlation}), the COTREC approach (described in section~\ref{s:Cross Correlation}), advection using only model windfield vectors, and finally against HRRR (described in section ~\ref{s:NWP}). The TREC approach was included in the study to further validate the necessity of the COTREC method. The model windfield approach was included in the study to show that the smoothest vectors do not lead to the highest prediction rates (for an image of the model windfields refer back to Fig.~\ref{f:wind vectors}).

%\begin{table} [tp] 
%	\begin{center}
 %   		\caption{Average Hourly Prediction Values over Two Day Period}
  %  		\label{Average Values}
%		\includegraphics[scale = 0.75]{/home/ncaban/Thesis_materials/images/myTable-crop}
 %   		%\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{/home/ncaban/Thesis_materials/images/myTable-crop}
%	\end{center}
%\end{table}
%give pictures of a single prediction for each method

%make image for predicted and actual outputs for the Texas hurricane
\begin{figure*}[p!]
	\begin{center}
		\begin{tabular} {@{} c c }
			\vspace{-5mm}
			\noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Texas_TREC_only_output.eps} & \noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Texas_COTREC_only_output.eps}\\			
			(a)  & (b) \\
			\vspace{-5mm}
			\noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Texas_model_only_output.eps} & \noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Texas_HRRR_only_output.eps}\\
			(c) & (d) \\
			\vspace{-5mm}
			\noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Texas_proposed_only_output.eps} & \noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Texas_real_only_output.eps}\\
			(e) & (f)
		\end{tabular}
		\caption{Hourly prediction for Hurricane Harvey 2017-08-28 at 0200 UTC. (a) TREC (b) COTREC (c) model (d) HRRR (e) proposed (f) observed location for 2017-08-28 at 0300 UTC}
		\label{f:Texas output}
	\end{center}
\end{figure*}

%make image for predicted and actual outputs for the Florida hurricane
\begin{figure*}[p!]
	\begin{center}
		\begin{tabular} {@{} c c }
			\vspace{-5mm}
			\noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Florida_TREC_only_output.eps} & \noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Florida_COTREC_only_output.eps}\\			
			(a)  & (b) \\
			\vspace{-5mm}
			\noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Florida_model_only_output.eps} & \noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Florida_HRRR_only_output.eps}\\
			(c) & (d) \\
			\vspace{-5mm}
			\noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Florida_proposed_only_output.eps} & \noindent\includegraphics[scale = 0.5]{/home/ncaban/Thesis_materials/images/Florida_real_only_output.eps}\\
			(e) & (f)
		\end{tabular}
		\caption{Hourly prediction for Hurricane Irma 2017-09-10 at 0200 UTC. (a) TREC (b) COTREC (c) model (d) HRRR (e) proposed (f) observed location for 2017-09-10 at 0300 UTC}
		\label{f:Florida output}
	\end{center}
\end{figure*}

The prediction outputs from each of the approaches as well as the true location of the storm can be seen in Figs.~\ref{f:Texas output} and \ref{f:Florida output}. Notice how in both of the figures the outputs from the COTREC and the proposed approach are almost exactly identical; this is because a majority of the vectors for the proposed approach are almost identical to those from the COTREC vectors. Also notice how the HRRR outputs for both cases but especially the output from Hurricane Irma is much more spread out and smoother than any of the other outputs. This can be attributed to the 3 km resolution of the HRRR output compared to the 1 km resolution of the mosaic from MRMS (described in Section~\ref{s:Mosaic}). The spreading out can be a way to improve the POD but it also greatly increases the FAR. As can be seen in Fig.~\ref{f:Evaluation for Texas} and Fig.~\ref{f:Evaluation for Florida}, the FAR values for the HRRR are generally far higher than any of the other prediction approaches. This leads to a decrease in the CSI measurements as well. 

As can be seen in Table~\ref{t:average}, The average hourly prediction measurements of the proposed approach slightly improves upon the COTREC approach. This means that the POD and CSI values are higher and the FAR values are lower. The improvements when compared to the approaches other than COTREC is drastic; for example the average CSI value of the proposed approach is ten percentage points greater than that of HRRR. The improvement over the HRRR method can be explained by the fact that extrapolation approaches perform much better than NWP approaches when the prediction time is less than approximately 2 hours \citep{Mandapaka++12,Lin++05}. 

Over 80\% of the time during the study, either the proposed approach or COTREC had the best performance in each category; on its own, the proposed approach had the best performance in each category at least 56\% of the time. Table~\ref{t:percentage} shows the percentage of the time during the study (85 hourly predictions were performed during the study) that the proposed approach had a better score than COTREC (better FAR values means the lower of the two). 

The highest recorded improvement in POD over COTREC was over 5 percentage points, in FAR the greatest improvement was over 6 percentage points, and in CSI the greatest improvement was over 4 percentage points; however there was a case where COTREC outperformed the proposed approach by over 5 percentage points. Generally though, the difference in skill between the proposed approach and COTREC was less than one percentage point. When comparing POD results, almost 32\% of the events had differences of less than one percentage point. When comparing FAR results, almost 37\% of the events had differences of less than one percentage point. When comparing CSI results, 40\% of the events had differences of less than one percentage point. 

When comparing the proposed approach to HRRR the highest recorded improvement in POD was over 34 percentage points and the proposed approach had a higher POD in over 85\% percent of the events. In terms of FAR, the highest recorded improvement was over 28 percentage points and the proposed approach had a lower FAR in over 97\% of the events. In terms of CSI, the highest recorded improvement was over 25 percentage points and the proposed approach had a lower FAR in over 94\% of the events. %are higher than those from HRRR 94\% of the time and there were 15 separate instances when the proposed approach's CSI values were 15 percentage points greater than those of HRRR.  %XXX - I like this stat but for now I do not know where to put it

%make image for the POD, FAR, and CSI values in the line graph
\begin{figure*}[p!]
	\begin{center}
		\begin{tabular} {@{} c }
			\vspace{-5mm}
			\noindent\includegraphics[width = 5.5in, height = 2.2in]{/home/ncaban/Thesis_materials/images/POD_texas.eps}\\	
			\vspace{3mm}		
			(a) \\
			\vspace{-5mm}
			\noindent\includegraphics[width = 5.5in, height = 2.2in]{/home/ncaban/Thesis_materials/images/FAR_texas.eps} \\
			\vspace{3mm}
			(b)\\
			\vspace{-5mm}
			\noindent\includegraphics[width = 5.5in, height = 2.2in]{/home/ncaban/Thesis_materials/images/CSI_texas.eps} \\
			\vspace{3mm}
			(c)
		\end{tabular}
		\caption{Evaluation parameters for Hurricane Harvey. (a) POD (b) FAR (c) CSI}
		\label{f:Evaluation for Texas}
	\end{center}
\end{figure*}

\begin{figure*}[p!]
	\begin{center}
		\begin{tabular} {@{} c }
			\vspace{-5mm}
			\noindent\includegraphics[width = 5.5in, height = 2.2in]{/home/ncaban/Thesis_materials/images/POD_florida.eps}\\
			\vspace{3mm}			
			(a) \\
			\vspace{-5mm}
			\noindent\includegraphics[width = 5.5in, height = 2.2in]{/home/ncaban/Thesis_materials/images/FAR_florida.eps} \\
			\vspace{3mm}
			(b)\\
			\vspace{-5mm}
			\noindent\includegraphics[width = 5.5in, height = 2.2in]{/home/ncaban/Thesis_materials/images/CSI_florida.eps} \\
			(c)
		\end{tabular}
		\caption{Evaluation parameters for Hurricane Irma. (a) POD (b) FAR (c) CSI}
		\label{f:Evaluation for Florida}
	\end{center}
\end{figure*}

% make a table for the average values over the entire time period
% Average Hourly Prediction Values over Two Day Period - caption for table
\begin{table} [tp]
   \begin{center}
	\begin{tabular}{|c c c c|}
		\hline
		\multicolumn{4}{|c|}{Hurricane Harvey} \\
		\hline
		\ & POD & FAR & CSI\\
		\hline
		\vspace{-4mm}
		Proposed & 0.4422 & 0.6610 & 0.2375\\
		\vspace{-4mm}
		COTREC & 0.4332 & 0.6760 & 0.2275\\
		\vspace{-4mm}
		TREC & 0.3200 & 0.7018 & 0.1813\\
		\vspace{-4mm}
		Model & 0.3926 & 0.6718 & 0.2172\\
		\vspace{-4mm}
		HRRR & 0.3078 & 0.8019 & 0.1353\\
		\multicolumn{4}{|c|}{\ } \\



		\multicolumn{4}{|c|}{Hurricane Irma}\\
		\hline
		\ &POD&FAR&CSI\\
		\hline
		\vspace{-4mm}
		Proposed&0.4765&0.6182&0.2708\\
		\vspace{-4mm}
		COTREC&0.4735&0.6217&0.268\\
		\vspace{-4mm}
		TREC&0.3196&0.6422&0.2029\\
		\vspace{-4mm}
		Model&0.3809&0.644&0.2269\\
		HRRR&0.3925&0.7364&0.1841\\
		\multicolumn{4}{|c|}{\ } \\


		\multicolumn{4}{|c|}{Overall}\\
		\hline
		\ &POD&FAR&CSI\\
		\hline
		\vspace{-4mm}
		Proposed&0.4591&0.6399&0.2539\\
		\vspace{-4mm}
		COTREC&0.4531&0.6491&0.2475\\
		\vspace{-4mm}
		TREC&0.3198&0.6723&0.1920\\
		\vspace{-4mm}
		Model&0.3869&0.6581&0.2220\\
		HRRR&0.3497&0.7696&0.1594\\
		\hline
	\end{tabular}
	\caption{Average hourly prediction values for each hurricane over the two day period as well as the entire study period}
	\label{t:average}
   \end{center}
\end{table}

\begin{table} [tp]
   \begin{center}
	\begin{tabular}{|c c |}
		\hline
		\vspace{-1mm}
		POD & 63.53\%\\
		\hline
		\vspace{-1mm}
		FAR & 65.88\%\\
		\hline
		CSI & 67.06\%\\

		\hline
	\end{tabular}
	\caption{Percentage of the time that the proposed approach had a higher success rate than COTREC in each respective category}
	\label{t:percentage}
   \end{center}
\end{table}

\section{Summary}
\label{s:Results Summary}

The results of the study were presented in this chapter. The study included results from five different prediction approaches: TREC, COTREC, HRRR, extrapolation using only the model windfields, and finally the proposed COTREC-model windfield hybrid approach. The proposed approach performed better than any of the other approaches 56\% of the time in each of the methods for measuring success. The study saw at least 63\% of the instances where the proposed approach outperformed COTREC based on the success measurements. Overall, the proposed approach had an average hourly POD improvement of 0.6 percentage points, a FAR improvement of 0.92 percentage points, and a CSI improvement of 0.64 percentage point over COTREC.

When compared to the HRRR, the proposed approach had a higher score in at least 85\% of the cases in each of the success metrics. Overall, the proposed approach had an average hourly POD improvement of 10.94 percentage points, a FAR improvement of 13.97 percentage points, and a CSI improvement of 9.45 percentage point over COTREC.
