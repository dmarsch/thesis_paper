
\section{General Background}
\label{s:GB}
Weather radars, such as the WSR-88D, are generally used in severe weather detection and prediction, flash flood warnings, and cloud microphysics and dynamics studies. Weather radar has a specific wavelength (as mentioned in the introduction, WSR-88D radars have a wavelength between 10 - 11 cm which is defined to be within in the S-band) as well as a specific pulse repetition frequency (PRF). For the WSR-88D two different PRFs are implemented: a short pulse which has a frequency between 318-1304 Hz as well as a long pulse which has a frequency between 318-452 Hz. These staggered PRFs are utilized to maximize the unambiguous range, or how far away the radar can detect precipitation echoes as well as maximize the  unambiguous velocity which is a measure of the speed of an object towards or away from the radar \citep{NEXRADtech}. The unambiguous velocity, which can be maximized with a high PRF, and unambiguous range, which can be maximized with a low PRF, can only be maximized by adversely affecting the other measurement \citep{Doviak++93}.%cite NEXRAD technical info

%maybe a section here about the range and azimuth data format
\begin{figure}[h]
	\begin{center}
		\begin{tabular}{@{} c } 
			%\noindent\includegraphics[scale = 0.45]{/home/ncaban/Thesis_materials/images/raw_radar.eps}
			\noindent\includegraphics[scale = 0.45]{/home/nick/Thesis_materials/images/raw_radar.eps}
		\end{tabular}
		\caption{Illustration showing a two-dimensional (showing a singular elevation angle) radar bin. The range gate and azimuth angle are shown for a visual representation. }
		\label{f:raw radar}
	\end{center}
\end{figure}

The level I (time series) radar data received by the  receiver comes in the format of complex data (composed of real and imaginary components) within each range gate, azimuth, elevation ordered triplet (also called a radar bin). Each range gate has a radial length of 250 m and the azimuth and elevation angle each span $1^{o} $ \citep{NEXRADtech}.  Fig.~\ref{f:raw radar} shows a two-dimensional depiction of a radar bin. The complex data is made up of the in-phase (I) and quadrature (Q) components. The phase difference between the I and Q components is $ 90^{o}$, which can be used to extract Doppler information when the returned phase falls into the "blind frequency". The combination of the I and Q channels prevents a loss in the signal strength of the received signal caused by analog to digital conversion sampling rate \citep{Skolnik++01}. Fig.~\ref{f:IQ} shows a simple example of I and Q data channels. 

\begin{figure}[t]
	\begin{center}
		\begin{tabular}{@{} c } 
			%\noindent\includegraphics[scale = 0.45]{/home/ncaban/Thesis_materials/images/raw_radar.eps}
			\noindent\includegraphics[scale = 0.45]{/home/nick/Thesis_materials/images/IQ.eps}
		\end{tabular}
		\caption{The In-Phase and Quadrature Data Channels }
		\label{f:IQ}
	\end{center}
\end{figure}

Pulse-Doppler radar is used because of its ability to determine the range of a target as well as extracting the Doppler moments \citep{Whiton++98b}. The Doppler moments are reflectivity, mean radial velocity, and spectrum width \citep{Doviak++93}. These moments can be defined in the following way:

\begin{itemize}
\item Reflectivity (Z): The average signal power received back from the radar beam reradiating off an object.
\item Mean Radial Velocity ($\hat{v}$): the rate at which an object moves towards or away from the radar. Note that objects moving orthogonally to the radar beam will have a radial velocity of zero.
\item Spectrum Width ($\hat{\sigma}_{v}$): How spread out the values of radial velocity are within one radar bin.
\end{itemize}
These moments can be calculated by using the autocorrelation function $R(l)$, which is a measure of similarity of a signal and a delayed version of the same signal. The equations are as follows:

\begin{equation}
\hat{Z} = \hat{R}(0) - N
\label{eq: reflectivity}
\end{equation}

\begin{equation}
\hat{v} = \frac{-v_{a}}{\pi} arg[\hat{R}(1) ]
\label{eq: velocity}
\end{equation}

\begin{equation}
\hat{\sigma_{v}} = \frac{v_{a}\sqrt{2}}{\pi} \sqrt{\left|\ln \left(\frac{\hat{R}(0) - N}{|\hat{R}(1)|}\right)\right|}
\label{spectrum width}
\end{equation}
where $ \hat{R}(0)$ is the autocorrelation with a lag of 0, N is the receiver noise power, $v_{a} $ is the the Nyquist velocity (maximum observable Doppler velocity) which is affected by the PRF mentioned earlier, and $arg[\hat{R}(1) ]$ is defined as the angle (in radians) between the I and Q channels. % I guess I have to define IQ channels 
Reflectivity is typically represented by units of $dBZ$ which can be calculated by $dBZ = 10 \log_{10} Z$.

As early as the 1970's, dual polarization (then known as orthogonal polarization because of the orientation of the beams) had been theorized to improve upon the conventional methods of precipitation classification. However, the polarized pulses would be sent out in an alternating fashion instead of simultaneously \citep{Seliga++76}. Fig.~\ref{f:dual_pol} shows the difference between conventional radar and dual polarization radar.

\begin{figure}[t]
	\begin{center}
		%\noindent\includegraphics[scale = 0.6]{/home/ncaban/Thesis_materials/images/Dual_Polarization.eps}
		\noindent\includegraphics[scale = 0.6]{/home/nick/Thesis_materials/images/Dual_Polarization.eps}
		\caption{Conventional vs. Dual Polarization radar. Notice how the dual polarization radar waves are orthogonal to each other. (Courtesy of NOAA (National Oceanic and Atmospheric Administration) and NWS (National Weather Service))}
		\label{f:dual_pol}
	\end{center}
\end{figure}
% I can provide what the dual polarization signals look like here (now how to credit the source of the picture...)
By 2000, simultaneous transmission and reception had been introduced. This method was compared to the methods of sequentially transmitting and simultaneously receiving as well as simultaneously transmitting and sequentially receiving \citep{Doviak++00}. Since then, dual polarization has been fully implemented in every NEXRAD radar. 

With the implementation of the dual polarization, four more variables became available, and they are: differential reflectivity, differential phase, specific differential phase, and correlation coefficient \citep{Istok++09}. These types of data can be defined in the following ways:

\begin{itemize}
\item Differential Reflectivity ($Z_{DR}$): a ratio of the reflectivity from the horizontal and vertical channels.
\item Differential Phase ($\phi_{DP}$): The phases for the horizontal and vertical polarizations are how much of a shift there is between the received signal and the transmitted (reference) signal. The differential phase is the difference between the vertical and horizontal phases.
\item Specific Differential Phase ($K_{DP}$): The rate of change in the differential phase as the radar signal travels through the atmosphere. Specific differential phase is utilized much more frequently than differential phase since it is a derived product and is insensitive to radar calibration.
\item Correlation Coefficient ($\rho_{HV}$ or RhoHV): a measure of similarity between successive vertical and horizontal pulses. 
\end{itemize} 
Dual polarization radar (also known as polarimetric radar) can be used to measure hydrometeors' size, shape, precipitation type (rain, heavy rain, hail, snow, etc.), and orientation. Together with the three Doppler moments, dual polarization radar has shown its advantages in hydrometeor classification, quantitative precipitation estimation, and radar data quality control.
% give a segue here to discuss the improvements used in the thesis

\section{Quantitative Precipitation Estimation}
%The nowcasting methods mentioned in Section~\ref{s:QPF} no longer use raw radar data. They now use radar data that has been converted into a single two dimensional or three dimensional mosaic image and has undergone quality control that includes removal of noise as well as precipitation correction. 
Radar based quantitative precipitation estimation (QPE) converts radar measurements into rainfall rates (mm hr $^{-1}$). In general, there are three major steps in QPE: radar quality control, mosaic image creation, and rainfall rate estimation. Details of these three steps are presented in the following sections.

\subsection{Radar Data Quality Control}
\label{s:QC}

Radar data quality control (QC) plays a crucial role in QPE, and those radar echoes caused by non-precipitation phenomena are removed using various methods developed during the existence of the NEXRAD network \citep{Tang++14, Lakshmanan++13, Park++09}. These phenomena come from biological sources such as birds or insects, ground clutter which can occur from objects such as mountains as well as buildings, and interference from electromagnetic sources such as telecommunications and sun spikes. \citet{Tang++14} propose a method to separate precipitation from non-precipitation radar echoes using polarimetric weather radar. Their proposed approach improves upon previous approaches that used a neural network \citep{Lakshmanan++13} and another that used fuzzy logic \citep{Park++09}.

\begin{figure*}[p!]
	\begin{center}
		\begin{tabular}{@{} c } 
			%\noindent\includegraphics[scale = 0.45]{/home/ncaban/Thesis_materials/images/Quality_Control.eps}
			\noindent\includegraphics[scale = 0.45]{/home/nick/Thesis_materials/images/Quality_Control.eps}
		\end{tabular}
		\caption{Before and after the quality control of the precipitation echoes. Courtesy of \citep{Tang++14}. (a) Raw Z (b) $ \rho_{HV}$ measurements (c) Z field after neural network\citep{Lakshmanan++13} (d) Z field after the $ \rho_{HV}$ filter (threshold = 0.95)}
		\label{f:quality control}		
	\end{center}
\end{figure*}
 
The approach describes a procedure where it initially thresholds the correlation coefficient ($\rho_{HV}$) (described in \ref{s:GB}), and retains the pixels above the threshold as well as pixels that meet other criteria. The threshold was set at 0.95 or a 95 \% similarity between successive horizontal and vertical pulses. It is “commonly known that pure rain and snow are associated with high $\rho_{HV}$ values close to 1 and that non-precipitation scatters generally produce low $\rho_{HV}$ values” \citep{Tang++14}. Pixels that are exempt from the correlation coefficient filter include hail regions as well as melting layer regions. The approach also attempts to further filter out reflectivities caused by spikes in interference, which typically have non uniform reflectivities in the horizontal and vertical directions. The approach then fills in small holes in precipitation echoes via a median filter. Fig.~\ref{f:quality control} shows the before and after effects of the quality control algorithm as well as the $ \rho_{HV}$ measurements used to filter out a majority of the non-precipitation echoes \citep{Tang++14}.

\subsection{Radar Data Mosaic}
\label{s:Mosaic}

%Maybe it is a good idea to talk about what the radar data looks like before bringing it all d



\citet{Zhang++16} developed a way to integrate the quality controlled radar data from every NEXRAD radar site into a single image, which they deemed a “mosaic image.” The mosaic image is produced by converting the radar data from its native spherical coordinates to Cartesian coordinates using a nearest-neighbor mapping scheme \citep{Zhang++05}. The multi-radar multi sensor (MRMS) network mosaic image has a spatial resolution of 1 km and a temporal resolution is 2 minutes. The output is a two-dimensional image where the data points of the image come from a combination of the lowest altitude data points that are not severely blocked of overlapping radar scan areas. Small gaps in the mosaic image are filled in using interpolation between the azimuth data, and larger gaps are filled in using the next lowest unblocked scan. This method increases the accuracy of the reflectivity result due to the fact that the lower altitude levels generally provide better predictions for “microphysical processes such as melting, evaporation, etc.” \citep{Zhang++16}. Figs.~\ref{f:Florida output} and \ref{f:Texas output} demonstrate the outputs of the MRMS  network using a threshold of 5 mm h$^{-1}$.

\subsection{Quantitative Precipitation Estimation WORK ON THIS PORTION}
\label{s:QPE}
Quantitative precipitation estimation (QPE) has been employed to convert the raw radar reflectivity into a rainrate value. This is done in an effort to warn of potential flash flood situations where approximate rainfall values are important. The QPE method proposed in \citep{Giangrande++08} estimates rainfall rates using the combination of reflectivity, differential reflectivity, specific differential phase $(K_{DP})$
which is immune to radar miscalibration since it is a relative measurement \citep{Zrnic++95}
, and correlation coefficient. The use of the polarimetric variables makes the estimation more robust with respect to drop size distribution (DSD) and hail. First, the method uses a classification algorithm employing a fuzzy logic approach with the radar variables and vertical temperature profile as inputs. The precipitation is then placed in one of the ten separate categories including light rain, heavy rain, and rain-hail to name a few. Each category had its own relationship based on one of the following three equations:

\begin{equation}
R(Z) = (1.7 \times 10^{-2}) Z^{0.714}
\label{eq:rainrate Z}
\end{equation}

\begin{equation}
R(K_{DP}) = 44 \left|K_{DP}\right|^{0.822} sign\left(K_{DP}\right)
\label{eq:rainrate K}
\end{equation}

\begin{equation}
R(Z,Z_{DR}) = (1.42 \times 10^{-2}) Z^{0.770} Z_{dr}^{-1.67}
\label{eq:rainrate Z_DR}
\end{equation}
where the units of Z is mm$^{6}$ m$^{-3}$ and  $Z_{dr}$ is a linear ratio (not in dB). \citep{Giangrande++08}.

These are not the only Z-R relationships currently in use. MRMS utilizes a different set of conditions. It should be noted that the R(Z) relations are different for convective and stratiform type precipitation. These equations are:

\begin{equation}
R_{stra} = max \left( 0.0365Z^{0.625},0.1155Z^{0.5} \right)
\label{eq:rainrate stratiform}
\end{equation}

\begin{equation}
R_{conv} = (1.7 \times 10^{-2}) Z^{0.714}
\label{eq:rainrate convective}
\end{equation}
Eq.~(\ref{eq:rainrate stratiform}) calculates two different rainfall rates and takes the larger rainfall rate as the official value.

In the MRMS system, the estimated rainfall rate is further corrected using the rain gauge measurements. These measurements provide a more accurate representation of the precipitation that reaches the ground. The differences in the estimation approaches can be attributed to variations in drop size, evaporation, as well as imperfect radar variables-precipitation rate relationships. \citep{Zhang++16,Wang++11}. 

\section{Summary}
\label{s:Background Summary}

The current weather radars, WSR-88D, are designed to extract Doppler and polarimetric measurements such as reflectivity, differential reflectivity, correlation coefficient, and specific differential phase that are used in quality control algorithms to differentiate between precipitation and non-precipitation as well as converting reflectivity into rainfall rates. The quality control algorithm outputs from all of the radars within the NEXRAD system are then used within the MRMS system that transforms the outputs into a single mosaic image. The mosaic reflectivity data is then converted to rainfall rates using the QPE algorithm.
