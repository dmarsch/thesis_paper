\contentsline {part}{ABSTRACT}{ii}
\contentsline {part}{ACKNOWLEDGEMENTS}{iii}
\contentsline {part}{LIST OF FIGURES}{vi}
\contentsline {part}{LIST OF TABLES}{viii}
\noindent \mbox {Chapter}\par 
\contentsline {chapter}{\numberline {1.}Introduction}{1}
\noindent \vskip \baselineskip \par 
\contentsline {section}{\numberline {1.1}Objective}{2}
\contentsline {section}{\numberline {1.2}Organization of Thesis}{2}
\contentsline {chapter}{\numberline {2.}Literature Review}{4}
\noindent \vskip \baselineskip \par 
\contentsline {section}{\numberline {2.1}General Background}{4}
\contentsline {section}{\numberline {2.2}Quantitative Precipitation Estimation}{8}
\contentsline {subsection}{\numberline {2.2.1}Radar data quality control}{8}
\contentsline {subsection}{\numberline {2.2.2}Radar data mosaic}{10}
\contentsline {subsection}{\numberline {2.2.3}Quantitative precipitation estimation }{11}
\contentsline {section}{\numberline {2.3}Summary}{12}
\contentsline {chapter}{\numberline {3.}Quantitative Precipitation Forecasting}{13}
\noindent \vskip \baselineskip \par 
\contentsline {section}{\numberline {3.1}Background}{13}
\contentsline {subsection}{\numberline {3.1.1}Numerical weather prediction}{13}
\contentsline {subsection}{\numberline {3.1.2}Radar based extrapolation}{15}
\contentsline {subsubsection}{\textbf {Cross Correlation.}}{15}
\contentsline {subsubsection}{\textbf {Centroid Tracking.}}{18}
\contentsline {subsection}{\numberline {3.1.3}NWP-Extrapolation hybrid}{20}
\contentsline {section}{\numberline {3.2}Proposed Fuzzy Logic Radar Based Extrapolation QPF Approach}{21}
\contentsline {subsection}{\numberline {3.2.1}Fuzzy sets and fuzzy logic}{22}
\contentsline {subsection}{\numberline {3.2.2}Model windfield vectors}{23}
\contentsline {subsection}{\numberline {3.2.3}Initial setup}{24}
\contentsline {subsection}{\numberline {3.2.4}Motion vector calculation}{25}
\contentsline {subsection}{\numberline {3.2.5}Fuzzy logic}{26}
\contentsline {subsection}{\numberline {3.2.6}COTREC implementation}{30}
\contentsline {subsection}{\numberline {3.2.7}Extrapolation using motion field}{34}
\contentsline {subsection}{\numberline {3.2.8}Dilation and smoothing}{36}
\contentsline {section}{\numberline {3.3}Summary}{39}
\contentsline {chapter}{\numberline {4.}Results and Analysis }{42}
\noindent \vskip \baselineskip \par 
\contentsline {section}{\numberline {4.1}Summary}{50}
\contentsline {chapter}{\numberline {5.}Conclusion and Future Works}{51}
\noindent \vskip \baselineskip \par 
\contentsline {section}{\numberline {5.1}Summary and Conclusions}{51}
\contentsline {section}{\numberline {5.2}Future Work}{52}
\noindent \vskip \baselineskip \par 
\contentsline {part}{REFERENCES}{54}
\contentsline {part}{APPENDICES}{58}
\vskip -\baselineskip 
\contentsline {chapter}{\numberline {A.}Successive Over Relaxation}{58}
\contentsline {chapter}{\numberline {B.}Segmotion}{60}
\noindent \vskip \baselineskip \par 
\contentsline {section}{\numberline {B.1}Overview}{60}
\contentsline {section}{\numberline {B.2}Segmotion algorithm}{60}
